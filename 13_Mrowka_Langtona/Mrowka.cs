﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13_Mrowka_Langtona
{
    class Mrowka
    {
        public enum Kierunk
        {
            gora, prawo, dol, lewo
        }
        private Color kolor;
        private int pozycja_x;
        private int pozycja_y;
        private Kierunk kierunek_mrowki;

        public Color Kolor
        {
            get { return kolor; }
        }
        public int Pozycja_x
        {
            get { return pozycja_x; }
            set { pozycja_x = value; }
        }
        public int Pozycja_y
        {
            get { return pozycja_y; }
            set { pozycja_y = value; }
        }
        public Kierunk Kierunek_mrowki
        {
            get { return kierunek_mrowki; }
            set { kierunek_mrowki = value; }
        }
        public Mrowka(Color k, int x, int y)
        {
            kolor = k;
            pozycja_x = x;
            pozycja_y = y;
            kierunek_mrowki = Kierunk.gora;
        }
    }
}
