﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13_Mrowka_Langtona
{
    class Plansza
    {
        private Color[,] dane;
        private List<Mrowka> mrowki;
        
        public Int32 RozmiarX
        {
            get { return dane.GetLength(0); }
        }
        public Int32 RozmiarY
        {
            get { return dane.GetLength(1); }
        }
        public List<Mrowka> Mrowki
        {
            get { return mrowki; }
        }
        public Color dajKolor(int x, int y)
        {
            return dane[x, y];
        }
        public Plansza(int rozmiarX, int rozmiarY)
        {
            dane = new Color[rozmiarX, rozmiarY];
            for (int i = 0; i < rozmiarX; i++)
            {
                for (int j = 0; j < rozmiarY; j++)
                {
                    dane[i, j] = Color.White;
                }
            }
            mrowki = new List<Mrowka>();
        }
        public void DodajMrowke(Mrowka m)
        {
            mrowki.Add(m);
        }

        public List<Point> Graj()
        {
            List<Point> zmienione = new List<Point>();
            foreach (Mrowka m in mrowki)
            {
                zmienione.AddRange(PrzesunMrowke(m));
            }
            return zmienione;
        }

        private List<Point> PrzesunMrowke(Mrowka m)
        {
            List<Point> zmienione = new List<Point>();
            if (m.Pozycja_x >= 0 && m.Pozycja_x < RozmiarX && m.Pozycja_y >= 0 && m.Pozycja_y < RozmiarY)
            {
                zmienione.Add(new Point(m.Pozycja_x, m.Pozycja_y));
                //w lewo
                if (dane[m.Pozycja_x, m.Pozycja_y] == Color.White)
                {
                    dane[m.Pozycja_x, m.Pozycja_y] = m.Kolor;
                    switch (m.Kierunek_mrowki)
                    {
                        case Mrowka.Kierunk.gora: m.Kierunek_mrowki = Mrowka.Kierunk.lewo; m.Pozycja_x -= 1; break;
                        case Mrowka.Kierunk.lewo: m.Kierunek_mrowki = Mrowka.Kierunk.dol; m.Pozycja_y += 1; break;
                        case Mrowka.Kierunk.dol: m.Kierunek_mrowki = Mrowka.Kierunk.prawo; m.Pozycja_x += 1; break;
                        case Mrowka.Kierunk.prawo: m.Kierunek_mrowki = Mrowka.Kierunk.gora; m.Pozycja_y -= 1; break;
                    }
                }
                //w prawo
                else
                {
                    dane[m.Pozycja_x, m.Pozycja_y] = Color.White;
                    switch (m.Kierunek_mrowki)
                    {
                        case Mrowka.Kierunk.gora: m.Kierunek_mrowki = Mrowka.Kierunk.prawo; m.Pozycja_x += 1; break;
                        case Mrowka.Kierunk.prawo: m.Kierunek_mrowki = Mrowka.Kierunk.dol; m.Pozycja_y += 1; break;
                        case Mrowka.Kierunk.dol: m.Kierunek_mrowki = Mrowka.Kierunk.lewo; m.Pozycja_x -= 1; break;
                        case Mrowka.Kierunk.lewo: m.Kierunek_mrowki = Mrowka.Kierunk.gora; m.Pozycja_y -= 1; break;
                    }
                }
                m.Pozycja_x = (m.Pozycja_x + RozmiarX) % RozmiarX;
                m.Pozycja_y = (m.Pozycja_y + RozmiarY) % RozmiarY;
                zmienione.Add(new Point(m.Pozycja_x, m.Pozycja_y));
            }
            return zmienione;
        }
    }
}
