﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _13_Mrowka_Langtona
{
    class Wizualizacja
    {
        private Plansza plansza;
        private PictureBox obrazek;
        private Int32 rozmiar;
        private Graphics grafika;
        public Wizualizacja(Plansza plansza, PictureBox obrazek, Int32 rozmiar)
        {
            this.plansza = plansza;
            this.obrazek = obrazek;
            this.rozmiar = rozmiar;

            obrazek.Size = new Size(plansza.RozmiarX * rozmiar, plansza.RozmiarY * rozmiar);
            obrazek.Image = new Bitmap(plansza.RozmiarX * rozmiar, plansza.RozmiarY * rozmiar);
            grafika = Graphics.FromImage(obrazek.Image);
        }


        public void Odrysuj()
        {
            for (int i = 0; i < plansza.RozmiarX; i++)
            {
                for (int j = 0; j < plansza.RozmiarY; j++)
                {
                    grafika.FillEllipse(new SolidBrush(plansza.dajKolor(i, j)), i * rozmiar, j * rozmiar, rozmiar, rozmiar);
                }
            }
            foreach (Mrowka m in plansza.Mrowki)
            {
                grafika.FillEllipse(new SolidBrush(Color.Black), m.Pozycja_x * rozmiar, m.Pozycja_y * rozmiar, rozmiar, rozmiar);
            }
            obrazek.Refresh();
        }

        public void OdrysujPozycje(Point p)
        {
            grafika.FillEllipse(new SolidBrush(plansza.dajKolor(p.X, p.Y)), p.X * rozmiar, p.Y * rozmiar, rozmiar, rozmiar);
            foreach (Mrowka m in plansza.Mrowki)
            {
                grafika.FillEllipse(new SolidBrush(Color.Black), m.Pozycja_x * rozmiar, m.Pozycja_y * rozmiar, rozmiar, rozmiar);
            }
            obrazek.Refresh();
        }
    }
}
