﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _13_Mrowka_Langtona
{
    public partial class Okno : Form
    {
        Plansza p;
        private Int32 kroki;
        Wizualizacja wizualizacja;
        public Okno()
        {
            InitializeComponent();
            kroki = 0;
            p = new Plansza(50, 100);
            p.DodajMrowke(new Mrowka(Color.Red, 5, 5));
            p.DodajMrowke(new Mrowka(Color.Green, 30, 40));
            p.DodajMrowke(new Mrowka(Color.Magenta, 40, 30));
            p.DodajMrowke(new Mrowka(Color.YellowGreen, 30, 60));

            wizualizacja = new Wizualizacja(p,pbPlanszaDuza, 7);

            //pierwsze wyrysowanie całej planszy, później tylko odrysowywanie pojedynczych pól
            wizualizacja.Odrysuj();
            trackBarSzybkosc_Scroll(null, null);
        }



        private void timer_Tick(object sender, EventArgs e)
        {
            kroki++;
            LabelKroki.Text = "Kroki=" + kroki;
            //metoda graj zwraca listę współrzędnych zmodyfikowanych (przemalowanych pól)
            List<Point> l = p.Graj();
            foreach (Point p1 in l)
            {
                //odrysowuja tylko zmienione pola na planszy
                wizualizacja.OdrysujPozycje(p1);
            }
            // z odrysowania całości zrezygnowałem bo chodziło za wolno
            //wizualizacja.Odrysuj();
        }

        private void trackBarSzybkosc_Scroll(object sender, EventArgs e)
        {
            timer.Interval = trackBarSzybkosc.Value;
        }
    }
}
