﻿namespace _13_Mrowka_Langtona
{
    partial class Okno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pbPlanszaDuza = new System.Windows.Forms.PictureBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.trackBarSzybkosc = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelKroki = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlanszaDuza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSzybkosc)).BeginInit();
            this.SuspendLayout();
            // 
            // pbPlanszaDuza
            // 
            this.pbPlanszaDuza.Location = new System.Drawing.Point(12, 60);
            this.pbPlanszaDuza.Name = "pbPlanszaDuza";
            this.pbPlanszaDuza.Size = new System.Drawing.Size(50, 50);
            this.pbPlanszaDuza.TabIndex = 0;
            this.pbPlanszaDuza.TabStop = false;
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // trackBarSzybkosc
            // 
            this.trackBarSzybkosc.Location = new System.Drawing.Point(56, 9);
            this.trackBarSzybkosc.Maximum = 1000;
            this.trackBarSzybkosc.Minimum = 10;
            this.trackBarSzybkosc.Name = "trackBarSzybkosc";
            this.trackBarSzybkosc.Size = new System.Drawing.Size(207, 45);
            this.trackBarSzybkosc.TabIndex = 1;
            this.trackBarSzybkosc.Value = 10;
            this.trackBarSzybkosc.Scroll += new System.EventHandler(this.trackBarSzybkosc_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(269, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "1000ms";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "10ms";
            // 
            // LabelKroki
            // 
            this.LabelKroki.AutoSize = true;
            this.LabelKroki.Location = new System.Drawing.Point(15, 40);
            this.LabelKroki.Name = "LabelKroki";
            this.LabelKroki.Size = new System.Drawing.Size(35, 13);
            this.LabelKroki.TabIndex = 4;
            this.LabelKroki.Text = "label3";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(463, 353);
            this.Controls.Add(this.LabelKroki);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.trackBarSzybkosc);
            this.Controls.Add(this.pbPlanszaDuza);
            this.Name = "Form1";
            this.Text = "Mrówka Langtona";
            ((System.ComponentModel.ISupportInitialize)(this.pbPlanszaDuza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSzybkosc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbPlanszaDuza;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.TrackBar trackBarSzybkosc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LabelKroki;
    }
}

